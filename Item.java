
/**
 * class Item - geef hier een beschrijving van deze class
 *
 * @author (jouw naam)
 * @version (versie nummer of datum)
 */
public class Item
{
    private String itemDescription;
    private String itemName;
    private int itemWeight;

    //Klasse constructor
    public Item(String itemDescription, String itemName, int itemWeight) {
        this.itemDescription = itemDescription;
        this.itemName = itemName;
        this.itemWeight = itemWeight;
    }

    public String setItemInformation()
    {
        return ".\nHey! That's a(n) " + itemName + "! You can use it to " + itemDescription + " and it weighs " + itemWeight + " kilos";
    }

    public String getItemDescription()
    {
        return itemDescription;
    }

    public int getItemWeight()
    {
        return itemWeight;
    }

    public String getItemName()
    {
        return itemName;   
    }

}
