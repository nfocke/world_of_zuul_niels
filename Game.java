/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */
import java.util.Stack;
public class Game 
{
    private Parser parser;
    private Room currentRoom;
    private Room previousRoom;
    private Stack<Room> back;
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createRooms();
        parser = new Parser();
        back = new Stack<Room>();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        Room classroom, lounge, chillarea, fddr, kitchen, mainhall, exit, janitorroom, toilets, entranceho, lobbyho, storage, secretlab, secretexit, outside;

        // create the rooms

        classroom = new Room("in the classroom");
        lounge = new Room("in the school lounge");
        chillarea = new Room("in the chill area");
        fddr = new Room("in the dispenser area");
        kitchen = new Room("in the kitchen");
        mainhall = new Room ("in the main hall");
        exit = new Room("in the main entrance and exit of the school");
        janitorroom = new Room("in the janitor area" );
        toilets = new Room("at the toilets");
        entranceho = new Room("in the entrance of the secret hideout");
        lobbyho = new Room("in the lobby of the hideout");
        storage = new Room("in the creepy storage area");
        secretlab = new Room("in the spooky secret laboratory");
        secretexit = new Room("in the secret exit of the building");
        outside = new Room("outside of the school, you made it!");

        // initialise room exits

        classroom.setExits("east", lounge);
        classroom.addItem("feed yourself and stay healthy", "Italiaanse bol", 1);

        lounge.setExits("east", chillarea);
        lounge.setExits("south", kitchen);
        lounge.setExits("west", classroom);

        chillarea.setExits("east", entranceho);
        chillarea.setExits("west", lounge);

        fddr.setExits("east", kitchen);

        kitchen.setExits("north", lounge);
        kitchen.setExits("south", mainhall);
        kitchen.setExits("west", fddr);

        mainhall.setExits("north", kitchen);
        mainhall.setExits("south", exit);

        exit.setExits("north", mainhall);
        exit.setExits("east", toilets);
        exit.setExits("south", janitorroom);
        exit.setExits("west", outside);

        janitorroom.setExits("north", exit);

        toilets.setExits("west", exit);

        entranceho.setExits("east", lobbyho);
        entranceho.setExits("west", chillarea);

        lobbyho.setExits("east", secretlab);
        lobbyho.setExits("south", storage);
        lobbyho.setExits("west", entranceho);

        storage.setExits("east", secretexit);
        storage.setExits("northeast", secretlab);
        storage.setExits("northwest", lobbyho);

        secretlab.setExits("south", storage);
        secretlab.setExits("west", lobbyho);

        secretexit.setExits("east", storage);
        secretexit.setExits("west", outside);

        outside.setExits("east", exit);
        outside.setExits("west", secretexit);

        currentRoom = classroom;  // start game in classroom
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("You woke up in a dark room that appears to be a school.");
        System.out.println("You notice many chairs and tables.");
        System.out.println("The room is awfully dark, it creeps you out a bit.");
        System.out.println("You decide to take a look around.");
        System.out.println("Because let's be honest, you don't have anything else to do.");
        System.out.println("And hey, you don't get the chance to explore");
        System.out.println("a dark, creepy, bad isolated school THAT often.");
        System.out.println("If you're not ridiculously intelligent, you can always");
        System.out.println("type 'help' for some hints.");
        System.out.println();
        System.out.println(currentRoom.getLongDescription());
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("I don't know what you mean...");
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go")) {
            goRoom(command);
        }
        else if (commandWord.equals("look")) {
            look();   
        }
        else if (commandWord.equals("study")) {
            study();   
        }
        else if (commandWord.equals("biem")) {
            biem();   
        }
        else if (commandWord.equals("goBack")) {
            goBack();
        }
        else if (commandWord.equals("quit")) {
            wantToQuit = quit(command);
        }

        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are lost. You are alone. You wander");
        System.out.println("around at the university.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println(parser.showCommands());
    }

    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();
        if(currentRoom.getExit(direction) != null) {
            Room nextRoom = currentRoom.getExit(direction);
            back.add(currentRoom);
            currentRoom = nextRoom;
            System.out.println(currentRoom.getLongDescription());
        }
        else {
            System.out.println("Yo man, you trying to use WalkThroughWalls?");
        }

    }

    private void look()
    {
        System.out.println(currentRoom.getLongDescription());   
    }

    private void study()
    {
        System.out.println("Added 1000 to your IQ.");
    }

    private void biem()
    {
        System.out.println("HOLY SCHNITZEL THE WHOLE ROOM EXPLODED! Besides that, nothing interesting happened.");   
    }

    private void goBack()
    {
        if(!back.empty()){
            currentRoom = back.pop();
        }
        System.out.println(currentRoom.getLongDescription());
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }

}
